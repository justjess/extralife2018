﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDHopeMeter : MonoBehaviour {

    public GameObject bar;
    public GameObject barContainer;
    public GameObject player;

    private float barStart;
    private float barMax;
    private float barCurrent;

	// Use this for initialization
	void Start () {
        barMax = player.GetComponent<player_controller>().maxJumpMeter;
        barStart = barMax;
        barCurrent = player.GetComponent<player_controller>().jumpMeter;
	}
	
	// Update is called once per frame
	void Update () {
        barStart = player.GetComponent<player_controller>().startingJumpMeter;
        bool dbgBtn = Input.GetButtonDown("DebugButton01");

        if (dbgBtn)
        {
            player.GetComponent<player_controller>().maxJumpMeter += 0.1f;
        }

        barMax = player.GetComponent<player_controller>().maxJumpMeter;

        float containerSize = (barMax / barStart) * 4;

        Debug.Log(containerSize);

        Vector3 locScale = barContainer.transform.localScale;
        locScale.x = containerSize;
        barContainer.transform.localScale = locScale;

        barMax = player.GetComponent<player_controller>().maxJumpMeter;
        barCurrent = player.GetComponent<player_controller>().jumpMeter;

        float barPerc = barCurrent / barMax;

        Vector3 barLocScale = bar.transform.localScale;
        barLocScale.x = barPerc;
        bar.transform.localScale = barLocScale;

    }
}
