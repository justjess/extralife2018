﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteHandler : MonoBehaviour {

    float range = 1;
    bool facingLeft;
    public GameObject PlayerCharacter;

	void Update () {
        float spriteFlip = Input.GetAxis("Horizontal");
        float xPos = spriteFlip * range;
        if (xPos <= -0.01f)
        {
            Vector3 spriteScale = PlayerCharacter.transform.localScale;
            spriteScale.x = -2.0f;
            PlayerCharacter.transform.localScale = spriteScale;
        }
        else if (xPos >= 0.01f)
        {
            Vector3 spriteScale = PlayerCharacter.transform.localScale;
            spriteScale.x = 2.0f;
            PlayerCharacter.transform.localScale = spriteScale;
        }
    }
}
