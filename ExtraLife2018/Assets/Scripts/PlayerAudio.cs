﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PlayerAudio : MonoBehaviour
{
    public player_controller pc;
    public AudioMixer PlayerSounds;
    public GameObject PlayerCharacter;
    public AudioSource jumpAudio;
    public AudioSource landAudio;
    public AudioClip landSound;
    public AudioClip jumpSound;
    public bool jumpHasPlayed;
    public bool landHasPlayed;

    //public bool jumpSoundHook;
    //public bool landSoundHook;

    private void Start()
    {
        //jumpSoundHook = PlayerCharacter.GetComponent<player_controller>().canJump;
        //landSoundHook = PlayerCharacter.GetComponent<player_controller>().isOnGround;
        jumpAudio = GetComponent<AudioSource>();
        landAudio = GetComponent<AudioSource>();
    }

    bool isLamp()
    {
        return Physics.Raycast(PlayerCharacter.transform.position, -Vector3.up, PlayerCharacter.GetComponent<player_controller>().distToGround, 1 << LayerMask.NameToLayer("Lamp"));
    }

    public void Update () {
        if (PlayerCharacter.GetComponent<player_controller>().isOnGround == false)
        {
            landHasPlayed = false;
        }

        if (PlayerCharacter.GetComponent<player_controller>().canJump == false && jumpHasPlayed == false)
        {
            jumpAudio.PlayOneShot(jumpSound, 1);
            jumpHasPlayed = true;
            landHasPlayed = false;
        }
        if (PlayerCharacter.GetComponent<player_controller>().isOnGround == true && landHasPlayed == false && isLamp() == false)
        {
            landAudio.PlayOneShot(landSound, 1);
            landHasPlayed = true;
            jumpHasPlayed = false;
        }
    }
}