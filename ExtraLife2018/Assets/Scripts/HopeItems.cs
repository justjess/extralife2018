﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopeItems : MonoBehaviour {

	void OnCollisionEnter (Collision col) {
        {
            if (col.gameObject.name == "HopeSphere")
            {
                Destroy(col.gameObject);
            }
        }
    }
}