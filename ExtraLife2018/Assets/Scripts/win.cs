﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class win : MonoBehaviour {

    public GameObject player;
    public GameObject winText;

    public AudioSource elevatorAudio;
    public AudioClip elevatorDing;
    public AudioMixer elevatorSounds;
    bool dingHasPlayed;

    public AudioClip takeElevator;
    bool takeHasPlayed;

	// Use this for initialization
	void Start () {
        winText.SetActive(false);
        elevatorAudio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Cancel") && !player.GetComponent<player_controller>().canPlay)
        {
            takeHasPlayed = false;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            elevatorAudio.PlayOneShot(takeElevator, 1);
            takeHasPlayed = true;
        }


        if (Input.GetAxis("Submit") > 0 && !player.GetComponent<player_controller>().canPlay)
        {
            Debug.Log("lmao you think you can escape");
            Application.Quit();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == player)
        {
            dingHasPlayed = false;
            player.GetComponent<player_controller>().canPlay = false;

            winText.SetActive(true);

            elevatorAudio.PlayOneShot(elevatorDing, 1);
            dingHasPlayed = true;
        }

    }
}
