﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class HopeCatchingScript : MonoBehaviour {

    public GameObject PlayerCharacter;
    public AudioMixer PlayerSounds;
    public AudioSource hopeAudio;
    public AudioClip hopeSound;
    public bool hopeHasPlayed;

    private void Start()
    {
        PlayerCharacter = GameObject.Find("Player");
    }


    private void Update()
    {
        
    }

    void OnCollisionEnter(Collision col)
    {
        GameObject.FindObjectOfType(typeof(CapsuleCollider));
        {
            if (col.gameObject.name.Contains("HopeSphere"))
            {
                hopeHasPlayed = false;
                Destroy(col.gameObject);
                player_controller player_Controller = PlayerCharacter.GetComponent<player_controller>();
                player_Controller.totalItems = player_Controller.totalItems + 0.5f;
                player_Controller.maxJumpMeter = player_Controller.startingJumpMeter + player_Controller.totalItems;

                if (hopeHasPlayed == false)
                {
                    hopeAudio.PlayOneShot(hopeSound, 1);
                    hopeHasPlayed = true;
                }
            }
        }
    }
}