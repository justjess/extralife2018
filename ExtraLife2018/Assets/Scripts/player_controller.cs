﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_controller : MonoBehaviour {

    public Rigidbody rb;
    //Jess's additions
    public GameObject PlayerCharacter;
    public Vector3 originPosition;
    //private float maxJumpHeight;
    //public float maxJumpOffset = 1.5f;
    public float velocityJump;
    public float totalItems = 0;
    public bool canJump;

    public float moveSpeed;
    public float jumpSpeed;
    public float startingJumpMeter;
    public float maxJumpMeter;
    public float jumpMeter;
    public float sprintMod;
    public float meterFillTimer = 1f;

    public float nonJumpTimer = 0;

    public float distToGround = 0.5f;

    public bool isOnGround = false;

    public bool canPlay = true;

    void Start () {
        //SetMaxHeight();
        jumpMeter = maxJumpMeter;
        canJump = true;
        velocityJump = 5;
        moveSpeed = 100;
        jumpSpeed = 150;
        sprintMod = 0.5f;
        maxJumpMeter = 4;
        startingJumpMeter = 4;
        jumpMeter = 4;
    }

    bool isGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, distToGround);
    }
	
	void Update () {
        if (canPlay)
        {
            isOnGround = isGrounded();
            float horiz = Input.GetAxis("Horizontal");
            float sprintButton = Input.GetAxis("Sprint");
            float jumpButton = Input.GetAxis("Jump");
            float velocityButton = Input.GetAxis("Velocity");

            if (canJump == true)
            {
                if (velocityButton > 0)
                {
                    //SetMaxHeight();
                    Vector3 vel = rb.velocity;
                    vel.y = velocityJump;
                    rb.velocity = vel;
                    canJump = false;
                }
                else
                {

                }
            }

            if (velocityButton > 0 && jumpMeter > 0)
            {
                jumpMeter = Mathf.Clamp(jumpMeter - (10 * Time.deltaTime), 0, maxJumpMeter);
                nonJumpTimer = 0;
                //Get current position, set Player max jump height to offset of that position
                //SetMaxHeight();
                rb.AddForce(Vector3.up * jumpSpeed * velocityButton);
            }
            else
            {
                nonJumpTimer += meterFillTimer * Time.deltaTime;
            }

            if (isGrounded())
            {
                nonJumpTimer = 10;
                canJump = true;
            }

            if (nonJumpTimer >= 10)
            {
                jumpMeter = Mathf.Clamp(jumpMeter + (10 * Time.deltaTime), 0, maxJumpMeter);
            }

            rb.AddForce(Vector3.right * (moveSpeed * (1 + sprintButton * sprintMod)) * horiz);
        }
    }

    /*
    private void SetMaxHeight()
    {
        originPosition.y = transform.position.y;
        maxJumpHeight = originPosition.y + maxJumpOffset;
    }
    
    private void FixedUpdate()
    {
        //If Player exceeds maxJumpHeight, set Player y position to maxJumpHeight
        if (PlayerCharacter.transform.position.y >= maxJumpHeight) {
            transform.position = new Vector3(transform.position.x,maxJumpHeight,transform.position.z);
        }
    }
    
    void OnCollisionEnter(Collision col)
    {
        GameObject.FindObjectOfType(typeof(CapsuleCollider));
        {
            if (col.gameObject.name.Contains("HopeSphere"))
            {
                Destroy(col.gameObject);
                totalItems = totalItems + 0.5f;
                maxJumpMeter = startingJumpMeter + totalItems;
            }
        }
    }
    */
}